



#### Run the SLA dag 

- checkout the `dags/sla_dag.py` 

  **in task defintion** : `sla=timedelta(seconds=5)`,


- copy `sla_dag.py` to dags folder and wait to appear in dag list.
- enable the dag
- check out the `SLA  Misses` / `Task Instances`



#### Get Alert on [SLACK](https://app.slack.com/client)

- [create a new workspace](https://slack.com/get-started#/create) (if you have not yet!)

- create a channel named `sla_miss`

- create new APP : [use this address](https://api.slack.com/apps?new_app=1) 

  - App Name : SLA_NIKAMOOZ
  - Development Slack Workspace
  - Incoming Webhooks: On ([read this guide](https://medium.com/datareply/integrating-slack-alerts-in-airflow-c9dcd155105)) 
  - Add New webhook to Workspace!
  - choose `sla_miss` in next screen and press `Allow`.
  - after finished, write down `Webhook URL` that sounds like this :

  ```bash
  https://hooks.slack.com/services/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
  ```

  **webhook address** : https://hooks.slack.com/services

  **password** : /T01QF93UE4T/B01QFA1B6V9/wMG2CYyZ2zcwRXlFh7TG0HAO

  

  - create a new connection in `Airflow UI ` 

    - **Name** : slack
    - **Conn Type** : http 
    - **Host** : https://hooks.slack.com/services
    - **password** :  /T01QF93UE4T/B01QFA1B6V9/wMG2CYyZ2zcwRXlFh7TG0HAO

    

#### Run the SLA dag with Slack Alerting 

- checkout the `dags/sla_dag_slack.py` 

  **in dag definition** : `sla_miss_callback=send_slack_alert_sla_miss` (optional)
  **in task defintion** : `sla=timedelta(seconds=5)`,




- copy `sla_dag.py` to dags folder and wait to appear in dag list.

- enable the dag

- check out the `SLA  Misses` / `Task Instances`

- check out the `Slack Channel` just created.

  



