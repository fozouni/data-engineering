#### Template and Macros

copy the `steps/8/dags` folder to corresponding folder in airflow 

- trigger the `dag_templates`
- check the logs
- trigger the `dag_templated_logs`

refer to [Apache Airflow Official Documentation](https://airflow.apache.org/docs/apache-airflow/2.0.1/macros-ref.html)

### Workflow of the DAG

Let me present you what our DAG does.

It is composed of 4 tasks, each of them using templates and macros in different ways.

1. First, a variable named **templated_log_dir** is built using the variable “source_path” we created earlier and some macros and variables to have a path according to the execution date of the DAG. This path will look like this once rendered: **<variable source_path>/data/macros(<execution_date>)/**
2. Then, task 1 generates logs using the BashOperator by calling a script named generate_new_logs.sh. This script creates new logs in the **templated_log_dir** foldergiven by the variable {{ ds }}. Basically we will end up with the following path: /usr/local/airflow/data/2019-01-01-00-00/log.csv
3. Task 2 checks that “log.csv” has been created as expected. To do so, it executes a bash command with the templated path where the file should be located.
4. Task 3 uses the PythonOperator to execute an external python script named “process_log.py” in order to process and clean “log.csv” using the library Pandas. **This script needs to know where the file is and so the templated path is given as parameter to the task**. Again, this path is rendered at runtime. The task produces a new file named “processed_log.csv”
5. Finally, once the task 3 is finished, task 4 creates a table corresponding to the data contained in “processed_log.csv”, gets the data and loads them into a PostgreSQL database. All of these steps are described in a script named insert_log.sql. Like the previous task, the SQL script needs to know where “processed_log.csv” is located. To do so, **we extend the PostgresOperator and override the variable template_fields to make the parameter “parameters” compatible with templates.**

