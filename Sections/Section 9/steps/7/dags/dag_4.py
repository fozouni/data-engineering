from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.dummy_operator import DummyOperator
from datetime import  datetime
with DAG(
	dag_id='tag_list_4',
	schedule_interval='@daily',
	start_date=datetime(2020, 1, 1, 10, 00, 00),
	catchup=False,
	tags=['data_lake']
) as dag:
	start_task = DummyOperator(task_id='start')	
	end_task = DummyOperator(task_id='end')
	start_task >> end_task