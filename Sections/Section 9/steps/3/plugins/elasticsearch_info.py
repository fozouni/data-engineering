# This is the class you derive to create a plugin
from airflow.plugins_manager import AirflowPlugin
import urllib.parse
from flask import Blueprint
from flask_appbuilder import expose, BaseView as AppBuilderBaseView

# Importing base classes that we need to derive
from airflow.hooks.base import BaseHook
from airflow.models.baseoperator import BaseOperatorLink

from elasticsearch_plugin.hooks.elasticsearch_hook import ElasticsearchHook
from elasticsearch_plugin.operators.elasticsearch_operator import MySqlToElasticsearchTransfer
from elasticsearch_plugin.menu_links.kibana_tweets import appbuilder_mitem


# A global operator extra link that redirect you to
# task logs stored in S3
class GoogleLink(BaseOperatorLink):
    name = "Google"

    def get_link(self, operator, dttm):
        return "https://www.google.com"

# A list of operator extra links to override or add operator links
# to existing Airflow Operators.
# These extra links will be available on the task page in form of
# buttons.
class ViewLogsLink(BaseOperatorLink):
    name = 'View Logs in Seperate Tab'
    operators = [MySqlToElasticsearchTransfer]

    def get_link(self, operator, dttm):
        
        s = f"http://localhost:8080/log?dag_id={operator.dag_id}&task_id={operator.task_id}&execution_date="+urllib.parse.quote_plus(str(dttm))
        return s




# Defining the plugin class
class AirflowTestPlugin(AirflowPlugin):
    name = "elasticsearch_plugin"
    hooks = [ElasticsearchHook]
    macros = []
    flask_blueprints = []
    appbuilder_views = []
    appbuilder_menu_items = [appbuilder_mitem]
    global_operator_extra_links = [GoogleLink(),]
    operator_extra_links = [ViewLogsLink(), ]