## Download the Dataset[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#download-dataset)



got `WSL` and Run the command:

```
wget  https://s3.amazonaws.com/menusdata.nypl.org/gzips/2021_11_01_07_02_47_data.tgz
```

Replace the link to the up to date link from http://menus.nypl.org/data if needed.
Download size is about 35 MB.

## Unpack the Dataset[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#unpack-dataset)

```
mkdir menus_data
tar -xvf 2021_11_01_07_02_47_data.tgz --directory menus_data
```

Uncompressed size is about 150 MB.

The data is normalized consisted of four tables:
\- `Menu` — Information about menus: the name of the restaurant, the date when menu was seen, etc.
\- `Dish` — Information about dishes: the name of the dish along with some characteristic.
\- `MenuPage` — Information about the pages in the menus, because every page belongs to some menu.
\- `MenuItem` — An item of the menu. A dish along with its price on some menu page: links to dish and menu page.

## Create the Tables[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#create-tables)

Make sure that the clickhouse-server is up & running (in `WSL`) :

```bash
$ sudo service clickhouse-server status
$ sudo service clickhouse-server start
```



Open `DBeaver` and create a connection to the  local `clickhouse`  server(or reconnect to the existent one). press `F3` to open up the script editor, then create a new database named `menus` and finally, create the following tables. 

>  make sure that this newly created table is selected at the top of the `DBeaver` script file .

![](.\Images\MenusERD.png)

We use [Decimal](https://clickhouse.com/docs/en/sql-reference/data-types/decimal/) data type to store prices.

```
create database menus;
use menus;
CREATE TABLE menus.dish
(
    id UInt32,
    name String,
    description String,
    menus_appeared UInt32,
    times_appeared Int32,
    first_appeared UInt16,
    last_appeared UInt16,
    lowest_price Decimal64(3),
    highest_price Decimal64(3)
) ENGINE = MergeTree ORDER BY id;

CREATE TABLE menus.menu
(
    id UInt32,
    name String,
    sponsor String,
    event String,
    venue String,
    place String,
    physical_description String,
    occasion String,
    notes String,
    call_number String,
    keywords String,
    language String,
    date String,
    location String,
    location_type String,
    currency String,
    currency_symbol String,
    status String,
    page_count UInt16,
    dish_count UInt16
) ENGINE = MergeTree ORDER BY id;

CREATE TABLE menus.menu_page
(
    id UInt32,
    menu_id UInt32,
    page_number UInt16,
    image_id String,
    full_height UInt16,
    full_width UInt16,
    uuid UUID
) ENGINE = MergeTree ORDER BY id;

CREATE TABLE menus.menu_item
(
    id UInt32,
    menu_page_id UInt32,
    price Decimal64(3),
    high_price Decimal64(3),
    dish_id UInt32,
    created_at DateTime,
    updated_at DateTime,
    xpos Float64,
    ypos Float64
) ENGINE = MergeTree ORDER BY id;
```

## Import the Data[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#import-data)

Upload data into ClickHouse, got to `menus_data` and run:

```
clickhouse-client --password  default123 --database menus --format_csv_allow_single_quotes 0 --input_format_null_as_default 0 --query "INSERT INTO dish FORMAT CSVWithNames" < Dish.csv
clickhouse-client --password  default123 --database menus --format_csv_allow_single_quotes 0 --input_format_null_as_default 0 --query "INSERT INTO menu FORMAT CSVWithNames" < Menu.csv
clickhouse-client --password  default123 --database menus --format_csv_allow_single_quotes 0 --input_format_null_as_default 0 --query "INSERT INTO menu_page FORMAT CSVWithNames" < MenuPage.csv
clickhouse-client --password  default123 --database menus --format_csv_allow_single_quotes 0 --input_format_null_as_default 0 --date_time_input_format best_effort --query "INSERT INTO menu_item FORMAT CSVWithNames" < MenuItem.csv
```

We use [CSVWithNames](https://clickhouse.com/docs/en/interfaces/formats/#csvwithnames) format as the data is represented by CSV with header.

We disable `format_csv_allow_single_quotes` as only double quotes are used for data fields and single quotes can be inside the values and should not confuse the CSV parser.

We disable [input_format_null_as_default](https://clickhouse.com/docs/en/operations/settings/settings/#settings-input-format-null-as-default) as our data does not have [NULL](https://clickhouse.com/docs/en/sql-reference/syntax/#null-literal). Otherwise ClickHouse will try to parse `\N` sequences and can be confused with `\` in data.

The setting [date_time_input_format best_effort](https://clickhouse.com/docs/en/operations/settings/settings/#settings-date_time_input_format) allows to parse [DateTime](https://clickhouse.com/docs/en/sql-reference/data-types/datetime/) fields in wide variety of formats. For example, ISO-8601 without seconds like '2000-01-01 01:02' will be recognized. Without this setting only fixed DateTime format is allowed.

#### Working With Menus Dataset 

let's issue some queries in cli.

```sql
$ clickhouse-client --password  default123
$ show tables;
$ describe table menu;
$ select id, place ,location, currency, date \
from menu limit 10 ;

$ select id, location,date, max(page_number) as page_count\
from menu m join menu_page mp on m.id=mp.menu_id \
group by id, location,date \
order by page_count desc\
limit 20;

```



### Denormalize the Data[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#denormalize-data)

Data is presented in multiple tables in [normalized form](https://en.wikipedia.org/wiki/Database_normalization#Normal_forms). It means you have to perform [JOIN](https://clickhouse.com/docs/en/sql-reference/statements/select/join/#select-join) if you want to query, e.g. dish names from menu items.

> For typical analytical tasks it is way more efficient to deal with pre-JOINed data to avoid doing `JOIN` every time. It is called "denormalized" data. 

We will create a table `menu_item_denorm` where will contain all the data JOINed together:

```
CREATE TABLE menus.menu_item_denorm\
ENGINE = MergeTree ORDER BY (dish_name, created_at)\
AS SELECT\
    price,\
    high_price,\
    created_at,\
    updated_at,\
    xpos,\
    ypos,\
    dish.id AS dish_id,\
    dish.name AS dish_name,\
    dish.description AS dish_description,\
    dish.menus_appeared AS dish_menus_appeared,\
    dish.times_appeared AS dish_times_appeared,\
    dish.first_appeared AS dish_first_appeared,\
    dish.last_appeared AS dish_last_appeared,\
    dish.lowest_price AS dish_lowest_price,\
    dish.highest_price AS dish_highest_price,\
    menu.id AS menu_id,\
    menu.name AS menu_name,\
    menu.sponsor AS menu_sponsor,\
    menu.event AS menu_event,\
    menu.venue AS menu_venue,\
    menu.place AS menu_place,\
    menu.physical_description AS menu_physical_description,\
    menu.occasion AS menu_occasion,\
    menu.notes AS menu_notes,\
    menu.call_number AS menu_call_number,\
    menu.keywords AS menu_keywords,\
    menu.language AS menu_language,\
    menu.date AS menu_date,\
    menu.location AS menu_location,\
    menu.location_type AS menu_location_type,\
    menu.currency AS menu_currency,\
    menu.currency_symbol AS menu_currency_symbol,\
    menu.status AS menu_status,\
    menu.page_count AS menu_page_count,\
    menu.dish_count AS menu_dish_count\
FROM menu_item\
    JOIN dish ON menu_item.dish_id = dish.id\
    JOIN menu_page ON menu_item.menu_page_id = menu_page.id\
    JOIN menu ON menu_page.menu_id = menu.id;
```

## Validate the Data[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#validate-data)

Query:

```
SELECT count() FROM menu_item_denorm;
```

Result:

```
┌─count()─┐
│ 1329175 │
└─────────┘
```

## Run Some Queries[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#run-queries)

### Averaged historical prices of dishes[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#query-averaged-historical-prices)

Query:

```
SELECT\
    round(toUInt32OrZero(extract(menu_date, '^\\d{4}')), -1) AS d,\
    count(),\
    round(avg(price), 2),\
    bar(avg(price), 0, 100, 100)\
FROM menu_item_denorm\
WHERE (menu_currency = 'Dollars') AND (d > 0) AND (d < 2022)\
GROUP BY d\
ORDER BY d ASC;
```

Result:

```
┌────d─┬─count()─┬─round(avg(price), 2)─┬─bar(avg(price), 0, 100, 100)─┐
│ 1850 │     618 │                  1.5 │ █▍                           │
│ 1860 │    1634 │                 1.29 │ █▎                           │
│ 1870 │    2215 │                 1.36 │ █▎                           │
│ 1880 │    3909 │                 1.01 │ █                            │
│ 1890 │    8837 │                  1.4 │ █▍                           │
│ 1900 │  176292 │                 0.68 │ ▋                            │
│ 1910 │  212196 │                 0.88 │ ▊                            │
│ 1920 │  179590 │                 0.74 │ ▋                            │
│ 1930 │   73707 │                  0.6 │ ▌                            │
│ 1940 │   58795 │                 0.57 │ ▌                            │
│ 1950 │   41407 │                 0.95 │ ▊                            │
│ 1960 │   51179 │                 1.32 │ █▎                           │
│ 1970 │   12914 │                 1.86 │ █▋                           │
│ 1980 │    7268 │                 4.35 │ ████▎                        │
│ 1990 │   11055 │                 6.03 │ ██████                       │
│ 2000 │    2467 │                11.85 │ ███████████▋                 │
│ 2010 │     597 │                25.66 │ █████████████████████████▋   │
└──────┴─────────┴──────────────────────┴──────────────────────────────┘
```

Take it with a grain of salt.

```sql
EXPLAIN
SELECT
    round(toUInt32OrZero(extract(menu_date, '^\\d{4}')), -1) AS d,
    count(),
    round(avg(price), 2),
    bar(avg(price), 0, 100, 100)
FROM menu_item_denorm
WHERE (menu_currency = 'Dollars') AND (d > 0) AND (d < 2022)
GROUP BY d
ORDER BY d ASC

Query id: b98d26cc-aef5-45d0-a6ba-95c25a19e4a5

┌─explain─────────────────────────────────────────────────────────────────────────────────┐
│ Expression (Projection)                                                                 │
│   MergingSorted (Merge sorted streams for ORDER BY)                                     │
│     MergeSorting (Merge sorted blocks for ORDER BY)                                     │
│       PartialSorting (Sort each block for ORDER BY)                                     │
│         Expression (Before ORDER BY)                                                    │
│           Aggregating                                                                   │
│             Expression (Before GROUP BY)                                                │
│               Filter (WHERE)                                                            │
│                 SettingQuotaAndLimits (Set limits and quota after reading from storage) │
│                   ReadFromMergeTree                                                     │
└─────────────────────────────────────────────────────────────────────────────────────────┘

10 rows in set. Elapsed: 0.004 sec.
```



### Burger Prices[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#query-burger-prices)

Query:

```
SELECT
    round(toUInt32OrZero(extract(menu_date, '^\\d{4}')), -1) AS d,
    count(),
    round(avg(price), 2),
    bar(avg(price), 0, 50, 100)
FROM menu_item_denorm
WHERE (menu_currency = 'Dollars') AND (d > 0) AND (d < 2022) AND (dish_name ILIKE '%burger%')
GROUP BY d
ORDER BY d ASC;
```

Result:

```
┌────d─┬─count()─┬─round(avg(price), 2)─┬─bar(avg(price), 0, 50, 100)───────────┐
│ 1880 │       2 │                 0.42 │ ▋                                     │
│ 1890 │       7 │                 0.85 │ █▋                                    │
│ 1900 │     399 │                 0.49 │ ▊                                     │
│ 1910 │     589 │                 0.68 │ █▎                                    │
│ 1920 │     280 │                 0.56 │ █                                     │
│ 1930 │      74 │                 0.42 │ ▋                                     │
│ 1940 │     119 │                 0.59 │ █▏                                    │
│ 1950 │     134 │                 1.09 │ ██▏                                   │
│ 1960 │     272 │                 0.92 │ █▋                                    │
│ 1970 │     108 │                 1.18 │ ██▎                                   │
│ 1980 │      88 │                 2.82 │ █████▋                                │
│ 1990 │     184 │                 3.68 │ ███████▎                              │
│ 2000 │      21 │                 7.14 │ ██████████████▎                       │
│ 2010 │       6 │                18.42 │ ████████████████████████████████████▋ │
└──────┴─────────┴──────────────────────┴───────────────────────────────────────┘
```

### Vodka[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#query-vodka)

Query:

```
SELECT
    round(toUInt32OrZero(extract(menu_date, '^\\d{4}')), -1) AS d,
    count(),
    round(avg(price), 2),
    bar(avg(price), 0, 50, 100)
FROM menu_item_denorm
WHERE (menu_currency IN ('Dollars', '')) AND (d > 0) AND (d < 2022) AND (dish_name ILIKE '%vodka%')
GROUP BY d
ORDER BY d ASC;
```

Result:

```
┌────d─┬─count()─┬─round(avg(price), 2)─┬─bar(avg(price), 0, 50, 100)─┐
│ 1910 │       2 │                    0 │                             │
│ 1920 │       1 │                  0.3 │ ▌                           │
│ 1940 │      21 │                 0.42 │ ▋                           │
│ 1950 │      14 │                 0.59 │ █▏                          │
│ 1960 │     113 │                 2.17 │ ████▎                       │
│ 1970 │      37 │                 0.68 │ █▎                          │
│ 1980 │      19 │                 2.55 │ █████                       │
│ 1990 │      86 │                  3.6 │ ███████▏                    │
│ 2000 │       2 │                 3.98 │ ███████▊                    │
└──────┴─────────┴──────────────────────┴─────────────────────────────┘
```

To get vodka we have to write `ILIKE '%vodka%'` and this definitely makes a statement.

### Caviar[ ](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#query-caviar)

Let's print caviar prices. Also let's print a name of any dish with caviar.

Query:

```
SELECT
    round(toUInt32OrZero(extract(menu_date, '^\\d{4}')), -1) AS d,
    count(),
    round(avg(price), 2),
    bar(avg(price), 0, 50, 100),
    any(dish_name)
FROM menu_item_denorm
WHERE (menu_currency IN ('Dollars', '')) AND (d > 0) AND (d < 2022) AND (dish_name ILIKE '%caviar%')
GROUP BY d
ORDER BY d ASC;
```

Result:

```
┌────d─┬─count()─┬─round(avg(price), 2)─┬─bar(avg(price), 0, 50, 100)──────┬─any(dish_name)──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│ 1090 │       1 │                    0 │                                  │ Caviar                                                                                                                              │
│ 1880 │       3 │                    0 │                                  │ Caviar                                                                                                                              │
│ 1890 │      39 │                 0.59 │ █▏                               │ Butter and caviar                                                                                                                   │
│ 1900 │    1014 │                 0.34 │ ▋                                │ Anchovy Caviar on Toast                                                                                                             │
│ 1910 │    1588 │                 1.35 │ ██▋                              │ 1/1 Brötchen Caviar                                                                                                                 │
│ 1920 │     927 │                 1.37 │ ██▋                              │ ASTRAKAN CAVIAR                                                                                                                     │
│ 1930 │     289 │                 1.91 │ ███▋                             │ Astrachan caviar                                                                                                                    │
│ 1940 │     201 │                 0.83 │ █▋                               │ (SPECIAL) Domestic Caviar Sandwich                                                                                                  │
│ 1950 │      81 │                 2.27 │ ████▌                            │ Beluga Caviar                                                                                                                       │
│ 1960 │     126 │                 2.21 │ ████▍                            │ Beluga Caviar                                                                                                                       │
│ 1970 │     105 │                 0.95 │ █▊                               │ BELUGA MALOSSOL CAVIAR AMERICAN DRESSING                                                                                            │
│ 1980 │      12 │                 7.22 │ ██████████████▍                  │ Authentic Iranian Beluga Caviar the world's finest black caviar presented in ice garni and a sampling of chilled 100° Russian vodka │
│ 1990 │      74 │                14.42 │ ████████████████████████████▋    │ Avocado Salad, Fresh cut avocado with caviare                                                                                       │
│ 2000 │       3 │                 7.82 │ ███████████████▋                 │ Aufgeschlagenes Kartoffelsueppchen mit Forellencaviar                                                                               │
│ 2010 │       6 │                15.58 │ ███████████████████████████████▏ │ "OYSTERS AND PEARLS" "Sabayon" of Pearl Tapioca with Island Creek Oysters and Russian Sevruga Caviar                                │
└──────┴─────────┴──────────────────────┴──────────────────────────────────┴─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```

At least they have caviar with vodka. Very nice.

## Online Playground[¶](https://clickhouse.com/docs/en/getting-started/example-datasets/menus/#playground)

The data is uploaded to ClickHouse Playground, [example](https://gh-api.clickhouse.com/play?user=play#U0VMRUNUCiAgICByb3VuZCh0b1VJbnQzMk9yWmVybyhleHRyYWN0KG1lbnVfZGF0ZSwgJ15cXGR7NH0nKSksIC0xKSBBUyBkLAogICAgY291bnQoKSwKICAgIHJvdW5kKGF2ZyhwcmljZSksIDIpLAogICAgYmFyKGF2ZyhwcmljZSksIDAsIDUwLCAxMDApLAogICAgYW55KGRpc2hfbmFtZSkKRlJPTSBtZW51X2l0ZW1fZGVub3JtCldIRVJFIChtZW51X2N1cnJlbmN5IElOICgnRG9sbGFycycsICcnKSkgQU5EIChkID4gMCkgQU5EIChkIDwgMjAyMikgQU5EIChkaXNoX25hbWUgSUxJS0UgJyVjYXZpYXIlJykKR1JPVVAgQlkgZApPUkRFUiBCWSBkIEFTQw==).