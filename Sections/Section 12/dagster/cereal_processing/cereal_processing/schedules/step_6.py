from datetime import datetime,time

from dagster import daily_schedule, schedule


@daily_schedule(
    pipeline_name="step4_pipeline",
    start_date=datetime(2021, 4, 20),
    execution_time=time(3, 45),
    execution_timezone="Asia/Tehran",
    mode='test'
)
def daily_cereals(date):
    run_config={"solids": {"read_csv": {"config": {"csv_name": "cereal.csv"} } } }
    return run_config

@schedule(
    cron_schedule="*/2 * * * *",
    pipeline_name="step6_pipeline",
    execution_timezone="Asia/Tehran",
    mode='test'
)
def my_2minitues_cereal_check(context):
    date = context.scheduled_execution_time.strftime("%Y-%m-%d")
    run_config={"solids": {"read_csv_step_6": {"config": {"csv_name": "cereal.csv"} } } }
    return  run_config
