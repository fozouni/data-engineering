from dagster import execute_pipeline
from cereal_processing.pipelines.cereals_sort_by_colories import cereals_sort_by_colories


def test_step2():
    result = execute_pipeline(cereals_sort_by_colories, mode="test")

    assert result.success
    assert len(result.output_for_solid("load_cereals")) == 77
    assert result.output_for_solid("sort_by_calories")[-1]["name"] == "Strawberry Fruit Wheats"