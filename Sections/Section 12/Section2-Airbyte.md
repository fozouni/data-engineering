### A Sample ETL (Mongo to Local CSV, Postgres)

##### Setup Airbyte

```bash
$ git clone https://github.com/airbytehq/airbyte.git
$ cd airbyte
$ docker-compose up
```



##### Setup Mongo

```bash
$ docker-compose -f .\docker-compose-mongo.yml
```

##### Get Some Data :

```json
$ curl -s 'https://randomuser.me/api/?results=5&nat=ir' |jq .results
[
  {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "محمدطاها",
      "last": "حیدری"
    },
    "location": {
      "street": {
        "number": 1130,
        "name": "پارک ولیعصر"
      },
      "city": "قم",
      "state": "زنجان",
      "country": "Iran",
      "postcode": 78709,
      "coordinates": {
        "latitude": "65.1685",
        "longitude": "-71.3979"
      },
      "timezone": {
        "offset": "+2:00",
        "description": "Kaliningrad, South Africa"
      }
    },
    "email": "mhmdth.hydry@example.com",
    "login": {
      "uuid": "c281fb6c-9389-4743-b235-86b720f1e078",
      "username": "bluegoose580",
      "password": "blue",
      "salt": "KnFJBqlS",
      "md5": "8691e621b051f1105e0b2a90b912b18b",
      "sha1": "665c254144d996f76a5d6176d2439d2d57e2e24c",
      "sha256": "55ef9eb80c89273d6f23e410e3ae8a8866fab3d4c9675e4677be0d18e98f4be1"
    },
    "dob": {
      "date": "1966-05-24T21:48:47.647Z",
      "age": 55
    },
    "registered": {
      "date": "2014-05-09T08:05:19.329Z",
      "age": 7
    },
    "phone": "010-91904539",
    "cell": "0947-023-9513",
    "id": {
      "name": "",
      "value": null
    },
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/44.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/44.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/44.jpg"
    },
    "nat": "IR"
  },
  {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "رضا",
      "last": "سالاری"
    },
    "location": {
      "street": {
        "number": 8777,
        "name": "شهید علی باستانی"
      },
      "city": "قدس",
      "state": "گیلان",
      "country": "Iran",
      "postcode": 71071,
      "coordinates": {
        "latitude": "-32.6014",
        "longitude": "158.8157"
      },
      "timezone": {
        "offset": "-5:00",
        "description": "Eastern Time (US & Canada), Bogota, Lima"
      }
    },
    "email": "rd.slry@example.com",
    "login": {
      "uuid": "6c035f81-5506-443e-bafa-aef2d22b31e8",
      "username": "bigswan564",
      "password": "buttercu",
      "salt": "N49j3jiT",
      "md5": "c60171174765c680042824d7c598d712",
      "sha1": "0697a1b826e686bc5da26ea10819b397019af312",
      "sha256": "91ba50df6fde55433d3751de539cf877d66c0c890d1122d9c1e16da8858cfabc"
    },
    "dob": {
      "date": "1966-09-13T22:20:25.566Z",
      "age": 55
    },
    "registered": {
      "date": "2013-12-02T00:28:48.161Z",
      "age": 8
    },
    "phone": "086-84241855",
    "cell": "0944-327-5728",
    "id": {
      "name": "",
      "value": null
    },
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/19.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/19.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/19.jpg"
    },
    "nat": "IR"
  }
]

```

or

```json
$ curl -H "User-Agent:Chrome/81.0" https://www.sahamyab.com/guest/twiter/list?v=0.1 | jq
{
      "id": "274285991",
      "sendTime": "2021-04-24T17:34:41Z",
      "sendTimePersian": "1400/02/04 22:04",
      "senderName": "یکتا",
      "senderUsername": "aliyekta1982",
      "senderProfileImage": "default",
      "content": "#ذوب \n https://ethprofitminer.com/?refer=561350\nسایت استخراج اتریوم:\nآدرس اتریوم رو در والت و یا بایننس کپی و به آدرس بالا وارد کنید",
      "type": "twit",
      "scoredPostDate": "1619285688654",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285945",
      "sendTime": "2021-04-24T17:34:24Z",
      "sendTimePersian": "1400/02/04 22:04",
      "senderName": "محمد",
      "senderUsername": "bagher13366",
      "senderProfileImage": "b8a97f56-3243-4a57-a2bb-e203c914c33b",
      "content": "#ورازی دوست عزیز در روند نزولی شاخص به هیچ عنوان نباید سهامداری کنی",
      "type": "twit",
      "scoredPostDate": "1619285671054",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285886",
      "sendTime": "2021-04-24T17:33:44Z",
      "sendTimePersian": "1400/02/04 22:03",
      "senderName": "شهریار",
      "senderUsername": "t04533",
      "senderProfileImage": "default",
      "content": "#برکت \nدوستان و سهامدارن عزیز برکت ، یک نصیحت دوستانه از طرف بنده :\nاز برکت نوسان نگیرید ، به هیچ وقت مجاز و فرصت نمیده که مجدد بیاید داخل ، هر لحظه منتظر خبر های خوب در این سهم باشید ، شماره ۴۰۳۰ وزارت بهداشت را اختصاص دادن به ثبت نام واکسن برکت👌👌👌👌👌👌👌",
      "type": "twit",
      "scoredPostDate": "1619285638925",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285883",
      "sendTime": "2021-04-24T17:33:44Z",
      "sendTimePersian": "1400/02/04 22:03",
      "senderName": "یکتا",
      "senderUsername": "aliyekta1982",
      "senderProfileImage": "default",
      "content": "#فولاد \n https://ethprofitminer.com/?refer=561350\nسایت استخراج اتریوم:\nآدرس اتریوم رو در والت و یا بایننس کپی و به آدرس بالا وارد کنید",
      "type": "twit",
      "scoredPostDate": "1619285631303",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285829",
      "sendTime": "2021-04-24T17:33:27Z",
      "sendTimePersian": "1400/02/04 22:03",
      "senderName": "یکتا",
      "senderUsername": "aliyekta1982",
      "senderProfileImage": "default",
      "content": "#فملی \n https://ethprofitminer.com/?refer=561350\nسایت استخراج اتریوم:\nآدرس اتریوم رو در والت و یا بایننس کپی و به آدرس بالا وارد کنید",
      "type": "twit",
      "scoredPostDate": "1619285614172",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285819",
      "sendTime": "2021-04-24T17:33:23Z",
      "sendTimePersian": "1400/02/04 22:03",
      "senderName": "Salar",
      "senderUsername": "hb123456789hb",
      "senderProfileImage": "6bb3d362-5447-4b5e-9b9f-4b073f149aab",
      "content": "#شتران جهت اطلاع دوستان گوشه ای از سوابق معاملاتی سال ۹۲",
      "type": "twit",
      "imageUid": "9c3bdaf7-0897-4e47-a676-4732e29b20e5",
      "mediaContentType": "image/png",
      "scoredPostDate": "1619285673583",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285788",
      "sendTime": "2021-04-24T17:33:07Z",
      "sendTimePersian": "1400/02/04 22:03",
      "senderName": "ابدنگال",
      "senderUsername": "abadangal",
      "senderProfileImage": "df48a039-31df-4f43-9e12-7e63cce8488d",
      "content": "#گشان خدا لعنتتون کنه. طرف از شیشلیک درست کردن حوصله ش سر رفته اومده یه همچین گندی زده که با آب زمزم پاک نمی شه.",
      "type": "twit",
      "scoredPostDate": "1619285594734",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285776",
      "sendTime": "2021-04-24T17:33:01Z",
      "sendTimePersian": "1400/02/04 22:03",
      "senderName": "یکتا",
      "senderUsername": "aliyekta1982",
      "senderProfileImage": "default",
      "content": "#برکت \n https://ethprofitminer.com/?refer=561350\nسایت استخراج اتریوم:\nآدرس اتریوم رو در والت و یا بایننس کپی و به آدرس بالا وارد کنید",
      "type": "twit",
      "scoredPostDate": "1619285588179",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285714",
      "sendTime": "2021-04-24T17:32:47Z",
      "sendTimePersian": "1400/02/04 22:02",
      "parentSendTime": "2021-04-24T14:19:14Z",
      "parentSendTimePersian": "1400/02/04 18:49",
      "parentId": "273881318",
      "parentSenderName": "شرکت تدبیرگران اقتصاد اوراسیا",
      "parentSenderUsername": "eurasia0economy",
      "parentSenderProfileImage": "b1b70e89-072e-4a1d-acec-2fb77245b153",
      "parentContent": "سلام\nدوست من تمام صحبهتای شما درست است\nاما وخارزم یک عیب بسیار بزرگ داره که محاسنش را کم رنگ می کند\nو ان ترکیب پرتفوی ان است\nسبد اصلی ان دو بانک در سبد بورسی و یک نیروگاه برق در سبد غیر بورسی ان است که وزن اصلی شرکت هستند\nهیچ کدام سه زیرمحموعه اصلی سود توزیعی ندارند\nاین سهم در 10 سال گذشته چه سود توزیعی داشته؟\nچقدر اصلا درامد داشته که به خواهد سود توزیع اش کند\n( البته امسال استثنا است چون در تابستان بورس حباب شده بود و شرکتهای سرمایه گذاری توانستند فروش خوبی از سرمایه داشته باشند)\nاگر چه تمام محاسنی که شما فرموده بودید درست است\nمن سعی کردم با گفتن معایب ان یک ترازوی خوب و بد با هم باشه\nخودم هم امروز به این سهم ورود کردم",
      "senderName": "مجتبی",
      "senderUsername": "mrshabani",
      "senderProfileImage": "default",
      "content": "#وخارزم یک نکته بسیار مهم اینه که شرکت های توسعه مهور مثل فولاد و فملی سود تقسیمی بالایی ندارند ولی گذشته نشون داده وقتی سودها دوباره سرمایه گذاری شدند درآمد شرکت ها چندین برابر شده. همین فولاد توی ۴ سال گذشته کلا ۱۰۰ تومن سود تقسیم نکرده ولی شما ببین توی ۴ سال گذشته ارزش سهم چند برابر شده. پس تقسیم سود بالا لزوما برای شرکت و سهامداراش مناسب ارزش افزوده ایجاد نمیکنه . مثلا شرکت های تامین سرمایه سود بالایی تقسیم میکنند ولی اصلا توسعه محور نیستند و گاهی برای نقدینگی وام میگیرند.\n\nبانکی ها توی دو سال گذشته بشدت متحول شدند. همین یک ماه گذشته وبصادر بخش بزرگی از پتروشیمی ها رو معادل ۱۸۰۰۰ میلیارد خرید. سهمی که ۳ سال پیش صحبت از ورشکستگیش بود و سال ۹۸ ۲۵۰۰ میلیارد تراز منفی داشت سال ۹۹ ۲۶۰۰ میلیارد تراز مثبت داشت. قطعا تراز ۱۴۰۰ دیدنی میشه. همینطور از سرمایه گذاری ها سود خیلی خوبی بدست میارند. جدای این موارد در صورت انجام توافق سهام بانکی بشدت رشد خواهند کرد.\n\nبرای بررسی بیشتر شما برید نمودار نسبت شاخص کل به شاخص بانکی توی ۱۰ سال گذشته رو بررسی کنید . ببین الان بسیار نزدیک هستیم که روند نزولی ۱۰ ساله بشکنه که قطعا میدونید شکست یک روند نزولی ۱۰ ساله یعنی چی. قطعا خریدهای سنگین حقوقی ها توی سهام بانکی بی دلیل نیست و رشد سهام بانکی که وزن بسیار بالایی توی پرتفوی وخارزم دارند میتونه این سهم رو وارد روند صعودی پرقدرت کنه.\n\nتحرکات حقوقی ها توی سهام بانکی مشخصه چرا دارند روی این گروه سرمایه گذاری میکنند.",
      "type": "quote",
      "finalPullDatePersian": ""
    },
    {
      "id": "274285689",
      "sendTime": "2021-04-24T17:32:35Z",
      "sendTimePersian": "1400/02/04 22:02",
      "parentSendTime": "2021-04-23T18:47:32Z",
      "parentSendTimePersian": "1400/02/03 23:17",
      "parentId": "273704848",
      "parentSenderName": "Robinhood",
      "parentSenderUsername": "roustapour",
      "parentSenderProfileImage": "538f11ed-3829-44fb-9790-2b71481e469c",
      "parentContent": "گزارش ماهیانه فروردین را حداکثر تا دو روز دیگه\nولی سالیانه رو تا قبل مجمع",
      "senderName": "javadjoon",
      "senderUsername": "javadjooni",
      "senderProfileImage": "default",
      "content": "امروز که نیامد انشاالله فردا",
      "type": "quote",
      "finalPullDatePersian": ""
    }
  
```

#### Airbyte UI 

- localhost:8000
- setup mongo to jsonl ETL pipeline

